package com.example.cameramlcomposem3.data.model

import android.graphics.Bitmap

data class CameraState(
    val capturedImage: Bitmap? = null
)
