package com.example.cameramlcomposem3.utils

import android.graphics.Bitmap
import android.graphics.Matrix

/* the rotation degrees is the rotation in degrees clockwise from the original orientation */
fun Bitmap.rotateBitmap(degrees: Int): Bitmap {
    val matrix = Matrix().apply {
        postRotate(-degrees.toFloat())
        postScale(-1f, -1f)
    }
    return Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
}